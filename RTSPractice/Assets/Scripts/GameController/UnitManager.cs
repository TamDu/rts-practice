﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public struct CommanderStats
{
	public Vector3 velocity;
	public float maxVelocity;
	public float minVelocity;
}

public class UnitManager : MonoBehaviour 
{
	public UnitMovementManager unitMoveMang;

	public List<GameObject> selectedUnits;	
	
	private void Awake()
	{
		if (unitMoveMang == null)
		{
			unitMoveMang = GetComponent<UnitMovementManager>();
		}
	}
	
	public void deselectUnit(GameObject selectedUnit)
	{
		if (selectedUnits.Contains(selectedUnit))
		{
			selectedUnit.GetComponent<Renderer>().material.color = Color.white;
			selectedUnits.Remove(selectedUnit);
		}
	}
	
	public void selectUnit(GameObject selectingUnit)
	{
		if (selectedUnits.Contains(selectingUnit) == false)
		{
			selectingUnit.GetComponent<Renderer>().material.color = Color.green;
			selectedUnits.Add(selectingUnit);
		}
	}
	
	public void moveSelectedUnits(Vector3 destination)
	{
		if (selectedUnits.Count == 0)
			return;
		for (int i = 0; i < selectedUnits.Count; i++)
		{
			UnitMover unitMoverComponent = selectedUnits[i].GetComponent<UnitMover>();
			if (unitMoverComponent != null)
			{
				setUnitDestination(destination);
				unitMoveMang.addUnitMover(unitMoverComponent);
			}
		}
	}
	
	public void setUnitDestination(Vector3 destination)
	{
		if (selectedUnits.Count != 0)
		{
			for (int i = 0; i < selectedUnits.Count; i++)
			{
				selectedUnits[i].GetComponent<UnitMover>().setDestination(destination);
			}
		}
	}
}
