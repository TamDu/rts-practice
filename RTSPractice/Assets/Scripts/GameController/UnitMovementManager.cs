﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UnitMovementManager : MonoBehaviour 
{
	public List<UnitMover> unitMovers;
	public UnitManager unitMang;

	private void Awake()
	{
		if (unitMang == null)
		{
			unitMang = GetComponent<UnitManager>();
		}
	}

	public void FixedUpdate()
	{
		if (unitMovers.Count != 0)
		{
			moveAllUnits();
		}
	}
	
	private void moveAllUnits()
	{
		for(int i = 0; i < unitMovers.Count; i++)
		{
			unitMovers[i].moveUnit();
		}
	}
	
	public void removeUnitMover(UnitMover unitMover)
	{
		if (unitMovers.Contains(unitMover))
		{
			unitMovers.Remove(unitMover);
		}
	}
	
	public void addUnitMover(UnitMover unitMover)
	{
		if (unitMovers.Contains(unitMover) == false)
		{
			unitMovers.Add(unitMover);
		}
	}
}
