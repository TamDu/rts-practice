﻿using UnityEngine;
using System.Collections;

public class Mover : MonoBehaviour 
{
	public float movementSpeed = 20f;
	public float rotationSpeed = 280f;
	protected Rigidbody myRgb;
	
	protected virtual void Awake()
	{
		myRgb = GetComponent<Rigidbody>();
	}
	
	protected void move(Vector3 direction, float speed)
	{
		direction.Normalize();
		transform.Translate(direction * speed * Time.deltaTime, Space.World);
	}
	
	protected void rotateUnit(Vector3 direction, float speed)
	{
		Quaternion newRotation = Quaternion.LookRotation(direction);
		transform.rotation = Quaternion.Slerp(transform.rotation, newRotation, Time.deltaTime * speed);
	}
}
