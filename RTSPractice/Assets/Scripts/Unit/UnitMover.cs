using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UnitMover : Mover 
{	
	protected Vector3 direction = Vector3.zero;
	protected Vector3 destination = Vector3.zero;
	public float minDestinDis = 1;
	public UnitManager unitMang;
	private bool moveAway = false;
	private Vector3 ahead = Vector3.zero;
	private List<GameObject> avoid_GO = new List<GameObject>();
	
	protected override void Awake ()
	{
		base.Awake ();
		unitMang = GameObject.FindGameObjectWithTag("GameController").GetComponent<UnitManager>();
	}

	private void FixedUpdate()
	{
		//debugControls();
	}
	
	private void debugControls()
	{
		Vector3 direction = Vector3.zero;
		if (Input.GetKey(KeyCode.W))
		{
			direction += transform.forward;
		}
		else if (Input.GetKey(KeyCode.S))
		{
			direction -= transform.forward;
		}
		
		if (Input.GetKey(KeyCode.A))
		{
			direction -= transform.right;
		}
		else if (Input.GetKey(KeyCode.D))
		{
			direction += transform.right;
		}
		
		if (direction != Vector3.zero)
		{
			moveUnit();
		}
	}
	
	public void moveUnit()
	{
		direction.y = 0.0f;
		ahead = direction * 3;
		checkAhead();
		direction.y = 0.0f;
//		if (moveAway)
//		{
//			move(moveAwayDir, movementSpeed);
//			rotateUnit(moveAwayDir, rotationSpeed);
//			destinationDistanceCheck();
//		}
//		else
		{
			move(direction, movementSpeed);
			rotateUnit(direction, rotationSpeed);
			destinationDistanceCheck();
		}
	}
	
	protected void destinationDistanceCheck()
	{
		Vector3 sqrOffset = destination - transform.position;
		float sqrDist = sqrOffset.sqrMagnitude;
		if (sqrDist < (minDestinDis * minDestinDis))
		{
			unitMang.unitMoveMang.removeUnitMover(this);
		}
	}
	
	public Vector3 getDestination()
	{
		return destination;
	}
	
	public void setDestination(Vector3 direction)
	{
		destination.y = 0;
		destination = direction;
		Vector3 dir = direction;
		dir.y = transform.position.y;
		dir = direction - transform.position;
		this.direction = dir;
	}
	
	private void checkAhead()
	{
		if (avoid_GO != null)
		{
			for (int i = 0; i < avoid_GO.Count; i++)
			{
				float dist = Vector3.Distance(avoid_GO[i].transform.position, transform.position);
				if (dist <= 2)
				{
					ahead = ahead - avoid_GO[i].transform.position;
					direction = direction + ahead;
					direction.Normalize();
				}
			}
		}
	}
	
	private void OnTriggerEnter(Collider colObj)
	{
		if (colObj.gameObject.tag == "Obstacle")
		{
			avoid_GO.Add(colObj.gameObject);
		}
	}
	
	private void OnTriggerExit(Collider colObj)
	{
		if (colObj.gameObject.tag == "Obstacle")
		{
			avoid_GO.Remove(colObj.gameObject);
		}
	}
}
