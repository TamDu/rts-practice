﻿using UnityEngine;
using System.Collections;

public class PcControls : MonoBehaviour 
{
	[Header("Mouse Controls")]
	public LayerMask mouseLayer;
	//length of raycast from screen
	public float rayDistance = 80;
	private UnitManager unitMang;
	
	private void Awake()
	{
		unitMang = GameObject.FindGameObjectWithTag("GameController").GetComponent<UnitManager>();
	}
	
	public void Update()
	{
		if (Input.GetKeyDown(KeyCode.Mouse1))
			mouseRayCast();
	}
	
	private void mouseRayCast()
	{
		Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit rayHit;
		if (Physics.Raycast(camRay, out rayHit, rayDistance, mouseLayer))
		{
			Vector3 pointHit = rayHit.point;
			unitMang.moveSelectedUnits(pointHit);
		}
	}
}
